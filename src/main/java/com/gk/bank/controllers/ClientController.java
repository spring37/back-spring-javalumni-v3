package com.gk.bank.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gk.bank.models.Client;
import com.gk.bank.services.ClientService;

@RestController
@RequestMapping("clients")

@CrossOrigin
public class ClientController {
	@Autowired
	private ClientService service;

	@GetMapping("")
	public List<Client> findAll(){
		return this.service.findAll();
	}
	
	@GetMapping("{id}")
	public Optional<Client> findById(@PathVariable Long id){
		return this.service.findById(id);
	}
	
	@GetMapping("utilisateur/{id}")
	public Optional<Client> findByIdUtilisateur(@PathVariable Long id){
		return this.service.findByIdUtilisateur(id);
	}

}
