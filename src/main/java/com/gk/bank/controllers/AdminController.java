package com.gk.bank.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gk.bank.models.Admin;
import com.gk.bank.services.AdminService;

@RestController
@RequestMapping("admins")

@CrossOrigin
public class AdminController {
	@Autowired
	private AdminService service;

	@GetMapping("")
	public List<Admin> findAll(){
		return this.service.findAll();
	}

}
