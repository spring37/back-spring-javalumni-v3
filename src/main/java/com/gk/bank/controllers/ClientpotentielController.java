package com.gk.bank.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gk.bank.models.Clientpotentiel;
import com.gk.bank.services.ClientpotentielService;

@RestController
@RequestMapping("clientspotentiels")

@CrossOrigin
public class ClientpotentielController {
	@Autowired
	private ClientpotentielService service;

	@GetMapping("")
	public List<Clientpotentiel> findAll(){
		return this.service.findAll();
	}

}
