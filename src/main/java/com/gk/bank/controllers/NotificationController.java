package com.gk.bank.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gk.bank.models.Notification;
import com.gk.bank.services.NotificationService;

@RestController
@RequestMapping("notifications")

@CrossOrigin
public class NotificationController {
	@Autowired
	private NotificationService service;

	@GetMapping("")
	public List<Notification> findAll(){
		return this.service.findAll();
	}

}
