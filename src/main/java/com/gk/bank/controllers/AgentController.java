package com.gk.bank.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gk.bank.models.Agent;
import com.gk.bank.services.AgentService;

@RestController
@RequestMapping("agents")

@CrossOrigin
public class AgentController {
	@Autowired
	private AgentService service;

	@GetMapping("")
	public List<Agent> findAll(){
		return this.service.findAll();
	}

}
