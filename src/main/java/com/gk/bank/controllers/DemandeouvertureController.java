package com.gk.bank.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gk.bank.models.Demandeouverture;
import com.gk.bank.services.DemandeouvertureService;

@RestController
@RequestMapping("demandesouvertures")

@CrossOrigin
public class DemandeouvertureController {
	@Autowired
	private DemandeouvertureService service;

	@GetMapping("")
	public List<Demandeouverture> findAll(){
		return this.service.findAll();
	}

}
