package com.gk.bank.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gk.bank.models.AuthDTO;
import com.gk.bank.models.Utilisateur;
import com.gk.bank.services.UtilisateurService;

@RestController
@RequestMapping("utilisateurs")

@CrossOrigin
public class UtilisateurController {
	
	@Autowired
	private UtilisateurService service;

	@GetMapping("")
	public List<Utilisateur> findAll(){
		return this.service.findAll();
	}
	
	@PostMapping("login")
	public Utilisateur authentification(@RequestBody AuthDTO entity ){
		return this.service.authentification(entity);
	}	
	
	@GetMapping("{id}")
	public Optional<Utilisateur> findById(@PathVariable Long id){
		return this.service.findById(id);
	}
}
