package com.gk.bank.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gk.bank.models.Transaction;
import com.gk.bank.services.TransactionService;

@RestController
@RequestMapping("transactions")

@CrossOrigin
public class TransactionController {
	@Autowired
	private TransactionService service;

	@GetMapping("")
	public List<Transaction> findAll(){
		return this.service.findAll();
	}
	
	@GetMapping("{id}")
	public Optional<Transaction> findById(@PathVariable Long id){
		return this.service.findById(id);
	}
	
	@GetMapping("compte/{id}")
	public List<Transaction> findByIdCompte(@PathVariable Long id){
		return this.service.findByIdCompte(id);
	}

}
