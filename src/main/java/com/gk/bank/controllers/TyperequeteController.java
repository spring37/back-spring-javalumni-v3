package com.gk.bank.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gk.bank.models.Typerequete;
import com.gk.bank.services.TyperequeteService;

@RestController
@RequestMapping("typesrequetes")

@CrossOrigin
public class TyperequeteController {
	@Autowired
	private TyperequeteService service;

	@GetMapping("")
	public List<Typerequete> findAll(){
		return this.service.findAll();
	}

}
