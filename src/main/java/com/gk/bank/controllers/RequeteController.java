package com.gk.bank.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gk.bank.models.Requete;
import com.gk.bank.services.RequeteService;

@RestController
@RequestMapping("requetes")

@CrossOrigin
public class RequeteController {
	@Autowired
	private RequeteService service;

	@GetMapping("")
	public List<Requete> findAll(){
		return this.service.findAll();
	}
	
	@PostMapping("")
	public Requete save(@RequestBody Requete entity){
		return this.service.save(entity);
	}

}
