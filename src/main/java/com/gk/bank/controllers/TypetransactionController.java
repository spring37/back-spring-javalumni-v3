package com.gk.bank.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gk.bank.models.Typetransaction;
import com.gk.bank.services.TypetransactionService;

@RestController
@RequestMapping("typestransactions")

@CrossOrigin
public class TypetransactionController {
	@Autowired
	private TypetransactionService service;

	@GetMapping("")
	public List<Typetransaction> findAll(){
		return this.service.findAll();
	}

}
