package com.gk.bank.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data

@Table(name = "clientpotentiel")
@NoArgsConstructor
@AllArgsConstructor
public class Clientpotentiel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idclientPotentiel;
    @Column()
	private String nom;
    @Column()
	private String prenom;
    @Column()
	private String email;
    @Column()
	private String adresse;
    @Column()
	private String telephone;
    @Column()
	private Double revenuMensuel;
    @Column()
	private String piecesJustif;
	public Long getIdclientPotentiel() {
		return idclientPotentiel;
	}
	public void setIdclientPotentiel(Long idclientPotentiel) {
		this.idclientPotentiel = idclientPotentiel;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public Double getRevenuMensuel() {
		return revenuMensuel;
	}
	public void setRevenuMensuel(Double revenuMensuel) {
		this.revenuMensuel = revenuMensuel;
	}
	public String getPiecesJustif() {
		return piecesJustif;
	}
	public void setPiecesJustif(String piecesJustif) {
		this.piecesJustif = piecesJustif;
	}
	
    
}
