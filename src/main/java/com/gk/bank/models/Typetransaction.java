package com.gk.bank.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data

@Table(name = "typetransaction")
@NoArgsConstructor
@AllArgsConstructor
public class Typetransaction {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idtypetransaction;
    @Column()
	private String type;
	public Long getIdtypetransaction() {
		return idtypetransaction;
	}
	public void setIdtypetransaction(Long idtypetransaction) {
		this.idtypetransaction = idtypetransaction;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	    
    
}
