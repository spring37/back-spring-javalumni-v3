package com.gk.bank.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data

@Table(name = "requete")
@NoArgsConstructor
@AllArgsConstructor
public class Requete {
	
	@Id
	@Column()
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idrequete;
    @Column()
	private String message;
    @Column()
	private Date date;



	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "client_idclient", nullable = true)
    private Client client;
	
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "typerequete_idtyperequete", nullable = true)
    private Typerequete typerequete;
	 
    
    
	public Long getIdrequete() {
		return idrequete;
	}


	public void setIdrequete(Long idrequete) {
		this.idrequete = idrequete;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	public Date getDate() {
		return date;
	}


	public void setDate(Date date) {
		this.date = date;
	}


	public Client getClient() {
		return client;
	}


	public void setClient(Client client) {
		this.client = client;
	}


	public Typerequete getTyperequete() {
		return typerequete;
	}


	public void setTyperequete(Typerequete typerequete) {
		this.typerequete = typerequete;
	} 
}
