package com.gk.bank.models;

import lombok.Data;

@Data
public class AuthDTO {
	private String email;
	private String mdp;
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMdp() {
		return mdp;
	}
	public void setMdp(String mdp) {
		this.mdp = mdp;
	}

	
}
