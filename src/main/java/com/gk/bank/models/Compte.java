package com.gk.bank.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data

@Table(name = "compte")
@NoArgsConstructor
@AllArgsConstructor
public class Compte {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idcompte;
    @Column()
	private String numCompte;
    @Column()
	private String rib;
    @Column()
	private Double solde;
    @Column()
	private Double decouvert;
    @Column()
	private Double montantAgios;
    @Column()
	private Double seuilRemuneration;
    @Column()
	private Double montantRemuneration;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "client_idclient", nullable = true)
    private Client client;

	public Long getIdcompte() {
		return idcompte;
	}

	public void setIdcompte(Long idcompte) {
		this.idcompte = idcompte;
	}

	public String getNumCompte() {
		return numCompte;
	}

	public void setNumCompte(String numCompte) {
		this.numCompte = numCompte;
	}

	public String getRib() {
		return rib;
	}

	public void setRib(String rib) {
		this.rib = rib;
	}

	public Double getSolde() {
		return solde;
	}

	public void setSolde(Double solde) {
		this.solde = solde;
	}

	public Double getDecouvert() {
		return decouvert;
	}

	public void setDecouvert(Double decouvert) {
		this.decouvert = decouvert;
	}

	public Double getMontantAgios() {
		return montantAgios;
	}

	public void setMontantAgios(Double montantAgios) {
		this.montantAgios = montantAgios;
	}

	public Double getSeuilRemuneration() {
		return seuilRemuneration;
	}

	public void setSeuilRemuneration(Double seuilRemuneration) {
		this.seuilRemuneration = seuilRemuneration;
	}

	public Double getMontantRemuneration() {
		return montantRemuneration;
	}

	public void setMontantRemuneration(Double montantRemuneration) {
		this.montantRemuneration = montantRemuneration;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}
	
	
}
