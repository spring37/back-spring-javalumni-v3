package com.gk.bank.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data

@Table(name = "typerequete")
@NoArgsConstructor
@AllArgsConstructor
public class Typerequete {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idtyperequete;
    @Column()
	private String type;
	public Long getIdtyperequete() {
		return idtyperequete;
	}
	public void setIdtyperequete(Long idtyperequete) {
		this.idtyperequete = idtyperequete;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	    
}
