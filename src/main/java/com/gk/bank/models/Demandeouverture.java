package com.gk.bank.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data

@Table(name = "demandeouverture")
@NoArgsConstructor
@AllArgsConstructor
public class Demandeouverture {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long iddemandeOuverture;
    @Column()
	private Date dateDemande;
    @Column()
	private int valide;//boolean pose probleme ?
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "clientpotentiel_idclientpotentiel", nullable = true)
    private Clientpotentiel clientpotentiel;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "admin_idadmin", nullable = true)
    private Admin admin;
	    
}
