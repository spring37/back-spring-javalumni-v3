package com.gk.bank.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gk.bank.models.Transaction;
import com.gk.bank.repositories.TransactionRepository;

@Service
public class TransactionService {


	@Autowired
	private TransactionRepository repository;
		
	public List<Transaction> findAll() {
		return this.repository.findAll();
	}

	public Optional<Transaction> findById(Long id) {
		return this.repository.findById(id);
	}
	
	public List<Transaction> findByIdCompte(Long id) {
		return this.repository.findByIdCompte(id);
	}

}