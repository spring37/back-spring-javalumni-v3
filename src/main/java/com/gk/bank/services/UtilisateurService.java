package com.gk.bank.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gk.bank.models.AuthDTO;
import com.gk.bank.models.Utilisateur;
import com.gk.bank.repositories.CustomRepository;
import com.gk.bank.repositories.UtilisateurRepository;

@Service
public class UtilisateurService {

	@Autowired
	private UtilisateurRepository repository;
	@Autowired
	private CustomRepository custrepository;
	
	public List<Utilisateur> findAll() {
		return this.repository.findAll();
	}
	
	// Pour faire la v�rif
	public Utilisateur authentification(AuthDTO auth) {
		return this.custrepository.authentification(auth);
	}
	
	public Optional<Utilisateur> findById(Long id) {
		return this.repository.findById(id);
	}
}
