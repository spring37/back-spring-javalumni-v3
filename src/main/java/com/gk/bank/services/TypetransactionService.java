package com.gk.bank.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gk.bank.models.Typetransaction;
import com.gk.bank.repositories.TypetransactionRepository;

@Service
public class TypetransactionService {


	@Autowired
	private TypetransactionRepository repository;
		
	public List<Typetransaction> findAll() {
		return this.repository.findAll();
	}

}