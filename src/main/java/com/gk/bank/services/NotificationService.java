package com.gk.bank.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gk.bank.models.Notification;
import com.gk.bank.repositories.NotificationRepository;

@Service
public class NotificationService {


	@Autowired
	private NotificationRepository repository;
		
	public List<Notification> findAll() {
		return this.repository.findAll();
	}

}