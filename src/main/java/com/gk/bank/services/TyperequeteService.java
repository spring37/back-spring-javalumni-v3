package com.gk.bank.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gk.bank.models.Typerequete;
import com.gk.bank.repositories.TyperequeteRepository;

@Service
public class TyperequeteService {


	@Autowired
	private TyperequeteRepository repository;
		
	public List<Typerequete> findAll() {
		return this.repository.findAll();
	}

}