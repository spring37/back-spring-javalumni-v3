package com.gk.bank.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gk.bank.models.Client;
import com.gk.bank.repositories.ClientRepository;

@Service
public class ClientService {


	@Autowired
	private ClientRepository repository;
		
	public List<Client> findAll() {
		return this.repository.findAll();
	}

	public Optional<Client> findById(Long id) {
		return this.repository.findById(id);
	}
	
	public Optional<Client> findByIdUtilisateur(Long id) {
		return this.repository.findByIdUtilisateur(id);
	}

}