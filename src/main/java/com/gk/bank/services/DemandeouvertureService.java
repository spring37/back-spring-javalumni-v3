package com.gk.bank.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gk.bank.models.Demandeouverture;
import com.gk.bank.repositories.DemandeouvertureRepository;

@Service
public class DemandeouvertureService {


	@Autowired
	private DemandeouvertureRepository repository;
		
	public List<Demandeouverture> findAll() {
		return this.repository.findAll();
	}

}