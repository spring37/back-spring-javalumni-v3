package com.gk.bank.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gk.bank.models.Agent;
import com.gk.bank.repositories.AgentRepository;

@Service
public class AgentService {


	@Autowired
	private AgentRepository repository;
		
	public List<Agent> findAll() {
		return this.repository.findAll();
	}

}