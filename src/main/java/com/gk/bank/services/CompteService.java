package com.gk.bank.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gk.bank.models.Compte;
import com.gk.bank.repositories.CompteRepository;

@Service
public class CompteService {


	@Autowired
	private CompteRepository repository;
		
	public List<Compte> findAll() {
		return this.repository.findAll();
	}

	public Optional<Compte> findById(Long id) {
		return this.repository.findById(id);
	}
	
	public List<Compte> findByIdClient(Long id) {
		return this.repository.findByIdClient(id);
	}

}