package com.gk.bank.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gk.bank.models.Clientpotentiel;
import com.gk.bank.repositories.ClientpotentielRepository;

@Service
public class ClientpotentielService {


	@Autowired
	private ClientpotentielRepository repository;
		
	public List<Clientpotentiel> findAll() {
		return this.repository.findAll();
	}

}