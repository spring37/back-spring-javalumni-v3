package com.gk.bank.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gk.bank.models.Requete;
import com.gk.bank.repositories.RequeteRepository;

@Service
public class RequeteService {


	@Autowired
	private RequeteRepository repository;
		
	public List<Requete> findAll() {
		return this.repository.findAll();
	}
	
	public Requete save(Requete entity) {
		return this.repository.save(entity);
	}

}