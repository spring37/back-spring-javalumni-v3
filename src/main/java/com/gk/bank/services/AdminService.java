package com.gk.bank.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gk.bank.models.Admin;
import com.gk.bank.repositories.AdminRepository;

@Service
public class AdminService {


	@Autowired
	private AdminRepository repository;
		
	public List<Admin> findAll() {
		return this.repository.findAll();
	}

}