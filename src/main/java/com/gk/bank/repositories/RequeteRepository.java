package com.gk.bank.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gk.bank.models.Requete;

@Repository
public interface RequeteRepository extends JpaRepository<Requete, Long>{ 
}
