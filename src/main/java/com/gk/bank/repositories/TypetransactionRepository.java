package com.gk.bank.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gk.bank.models.Typetransaction;

@Repository
public interface TypetransactionRepository extends JpaRepository<Typetransaction, Long>{
}
