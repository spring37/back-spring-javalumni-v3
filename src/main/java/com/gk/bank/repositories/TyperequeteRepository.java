package com.gk.bank.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gk.bank.models.Typerequete;

@Repository
public interface TyperequeteRepository extends JpaRepository<Typerequete, Long>{ 
}
