package com.gk.bank.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gk.bank.models.Clientpotentiel;

@Repository
public interface ClientpotentielRepository extends JpaRepository<Clientpotentiel, Long>{  
}
